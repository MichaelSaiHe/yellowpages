﻿using System;
using System.Collections.Generic;

namespace YellowPages
{
    class Program
    {
        static void Main(string[] args)
        {
            Person[] contacts = new Person[6];
            contacts[0]= new Person("Lars-Martin Solbak", "12345678", "Kanalveien 13", 5070);
            contacts[1] = new Person("Sindre Kjær", "98765432", "Kanalveien 14", 5072);
            contacts[2] = new Person("Marius Lode", "33333333", "Kanalveien 15", 5071);
            contacts[3] = new Person("Greg Linklater", "11111111", "Kanalveien 13B", 5072);
            contacts[4] = new Person("Philip Aubert", "99999999", "Kanalveien 13C", 5069);
            contacts[5] = new Person("Christopher Toussi", "77777777", "Kanalveien 10", 5060);

            Console.WriteLine("Please enter a search term:");
            string input = Console.ReadLine();
            List<Person> result = Search(input, contacts);
            foreach (Person p in result)
            {
                Console.WriteLine(p.ToString());
            }
        }

        public static List<Person> Search(string term, Person[] directory)
        {
            List<Person> results = new List<Person>();
            Person temp;
            foreach (Person contact in directory)
            {
                temp = contact.Find(term);
                if (temp != null)
                {
                    results.Add(temp);
                }
            }
            return results;
        }
    }
}
