﻿using System;
namespace YellowPages
{
	public class Person
	{
		private string FirstName;
		private string LastName;
		private string Phone;
		private string Address;
		private int Postcode;

		public Person(string name) {
			String[] fullName = name.Split(new Char[] { ' ' });
			//Console.WriteLine("firstname: " + fullName[0] + "lastname: " + fullName[^1]);
			FirstName = fullName[0];
			LastName = fullName[^1];
		}

		public Person(string name, string phone){
			String[] fullName = name.Split(new Char[] { ' ' });
			//Console.WriteLine("firstname: " + fullName[0] + "lastname: " + fullName[^1]);
			FirstName = fullName[0];
			LastName = fullName[^1];
			Phone = phone;

		}
		public Person(string name, string phone, string address, int postcode){
			String[] fullName = name.Split(new Char[] {' '});
			//Console.WriteLine("firstname: "+ fullName[0] + "lastname: " + fullName[^1]);
			FirstName = fullName[0];
			LastName = fullName[^1];
			Phone = phone;
			Address = address;
			Postcode = postcode;
		}

		public string GetFirstName(){
			return FirstName;
		}
		public string GetLastName()
		{
			return LastName;
		}
		public string GetAddress()
		{
			return Address;
		}
		public string GetPhone()
		{
			return Phone;
		}
		public int GetPostcode()
		{
			return Postcode;
		}

		public Person Find(string term){
			if (FirstName.Contains(term) || LastName.Contains(term) || Phone.Contains(term) || Address.Contains(term) || Postcode.ToString().Contains(term))
			{
				return new Person(FirstName + " " + LastName, Phone, Address, Postcode);
			}
			else return null;
		}

		public override string ToString(){
			return ""+(FirstName + " " + LastName + ", " + Phone + ", " + Address +", " + Postcode);
		}
	}
}
