## Yellowpages

Quick person and address lookup, implemented very simply. 


### v2 changes: 

Moduralized the program. This made it much more complicated with the same functionality.


### v3 changes:

Implemented Person objects and updated search functions to search for all fields within the Person objects. Changed the application to use Person objects instead of a hardcoded array.  